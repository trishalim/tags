## URL
www.tags.trishalim.com

## Technologies
Angular, Bootstrap

## Scripts
### `ng serve`

Runs the app in development mode.

### `ng build --prod`

Builds the app for production to the `dist/` directory.

## Features
1. Add tags.
2. Display tags (red for positives and zeroes, blue for negatives).
3. Edit tags.
4. Delete tags.