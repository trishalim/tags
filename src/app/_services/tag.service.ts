import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  // array of tags added
  tags = new BehaviorSubject([]);

  constructor() {
    // initialize this.tags if there are tags in localStorage
    const localStorageTags = JSON.parse(localStorage.getItem('tags'));
    this.tags.next(localStorageTags ? localStorageTags : []);
  }

  getTags(): Observable<Array<number>> {
    return this.tags.asObservable();
  }

  // remove a tag given index in array
  removeTag(index: number) {
    const tags = this.tags.value;
    tags.splice(index, 1);
    this.updateTags(tags);
  }

  /**
   * Converts a string of integers into an array of integers
   * @param {string} text         text input of tags that are separated by
                                  comma, semicolon or line-break
   * @param {boolean} resetTags   true if tags should be set back to empty
                                  before adding more tags
   */
  addTextAsTags(text: string, resetTags = false) {
    // split text using delimeters (comma, semicolon, line-break)
    const tags = text.split(/[\s,;\n]+/);

    // array of integer tags
    // reset the tags array to empty if resetTags is set to true
    let numberTags = resetTags ? [] : this.tags.value;

    for (let tag of tags) {
      // convert tag string to integer
      const numberTag = parseInt(tag);

      // add to array of tags if it is an integer
      if (tag && !isNaN(numberTag)) {
        numberTags.push(numberTag);
      }
    }

    this.updateTags(numberTags);
  }

  updateTags(tags: any[]) {
    this.tags.next(tags);
    localStorage.setItem("tags", JSON.stringify(tags));
  }

}
