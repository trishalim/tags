import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AddTagsComponent } from './add-tags/add-tags.component';
import { EditTagsComponent } from './edit-tags/edit-tags.component';

@NgModule({
  declarations: [
    AppComponent,
    AddTagsComponent,
    EditTagsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
